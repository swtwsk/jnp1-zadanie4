#ifndef BAKERY_H
#define BAKERY_H

#include <type_traits>
#include <tuple>
#include <utility>

#include "pie.h"
#include "cake.h"

template<class C, class A, A shelfArea, class... P>
class Bakery {
public:
    Bakery() = delete;

    Bakery(P... products) : profits_(0),
                            productsTuple_(std::move(std::tuple<P...>(products...))) {
        static_assert(sizeof...(P) > 0, "No products in the bakery");
        static_assert(std::is_floating_point_v<C>, "C is not a floating point");
        static_assert(std::is_integral_v<A>, "A is not an integral");
        static_assert(UniqueTypes<P...>::value, "Types are not unique");
        static_assert(computeAreaSpace(products...) <= shelfArea, "Not enough shelf area");
        static_assert(SameDimTypes<P...>::value, "Different dimension types");
        static_assert(SameDimTypesAsBakery<A, P...>::value, "Different dimension types");
        static_assert(SamePriceTypes<C, P...>::value, "Different price types");
    }

    C getProfits() {
        return profits_;
    }

    template<class Product>
    void sell() {
        static_assert(HasType <Product,
                      std::tuple<P...>>::value, "Product to sell is not in the bakery");
        Product & p = std::get<Product>(productsTuple_);
        static_assert(
                isAppleV<typename Product::dessertType> || isCreamV<typename Product::dessertType>,
                "Product is not for sale");
        if (p.getStock() > 0) {
            p.sell();
            profits_ += p.getPrice();
        }
    }

    template<class Product>
    int getProductStock() {
        static_assert(HasType <Product,
                      std::tuple<P...>>::value, "Product is not in the bakery");
        Product & p = std::get<Product>(productsTuple_);
        return p.getStock();
    }

    template<class Product>
    void restock(int additionalStock) {
        static_assert(HasType <Product,
                      std::tuple<P...>>::value, "Product is not in the bakery");
        static_assert(IsApple<typename Product::dessertType>::value,
                      "Product to restock is not an Apple Pie");
        Product & p = std::get<Product>(productsTuple_);
        p.restock(additionalStock);
    }

private:
    C profits_;
    std::tuple<P...> productsTuple_;

    /* Unique types */
    template<class T, class... Ts>
    static constexpr bool duplicated = std::disjunction_v<std::is_same<T, Ts>...>;

    template<class... Ts>
    struct UniqueTypes {
        constexpr static bool value = true;
    };

    template<class T, class... Ts>
    struct UniqueTypes<T, Ts...> {
        constexpr static bool value = !duplicated<T, Ts...> && UniqueTypes<Ts...>::value;
    };

    /* Same dimension types */
    template<class T, class... Ts>
    struct SameDimTypes {
        constexpr static bool value = std::conjunction_v<SameDimTypes<T, Ts>...>;
    };

    template<class T1, class T2>
    struct SameDimTypes<T1, T2> {
        constexpr static bool value = std::is_same_v<typename T1::dimType, typename T2::dimType>;
    };

    template<class BDT, class T, class... Ts>
    struct SameDimTypesAsBakery {
        constexpr static bool value = std::is_same_v<BDT, typename T::dimType>;
    };

    /* Same price types */
    template<class T, class... Ts>
    struct SamePriceTypes {
        constexpr static bool value = std::conjunction_v<SamePriceTypes<T, Ts>...>;
    };

    template<class T1, class T2>
    struct SamePriceTypes<T1, T2> {
        constexpr static bool value =
                (!isAppleV<typename T2::dessertType> && !isCreamV<typename T2::dessertType>) ?
                true : std::is_same_v<T1, typename T2::priceType>;
    };

    /* Compute needed space */
    template<class T, class... Ts>
    constexpr static double computeAreaSpace(T & product, Ts &... products) {
        return product.getArea() + computeAreaSpace(products...);
    }

    constexpr static double computeAreaSpace() {
        return 0.0;
    }

    /* Check if product is in the bakery */
    template<typename T, typename Tuple>
    struct HasType;

    template<typename T, typename... Us>
    struct HasType<T, std::tuple<Us...>> {
        constexpr static bool value = std::disjunction_v<std::is_same<T, Us>...>;
    };
};

#endif // BAKERY_H
