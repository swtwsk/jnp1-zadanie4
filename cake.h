#ifndef CAKE_H
#define CAKE_H

#include <type_traits>
#include <cassert>

class Cream;
class Cheese;

template<class T>
struct IsCream {
    constexpr static bool value = false;
};

template<>
struct IsCream<Cream> {
    constexpr static bool value = true;
};

template<class T>
constexpr bool isCreamV = IsCream<T>::value;

template<class T, T length, T width, class P, class CakeType>
class Cake {
public:
    typedef T dimType;
    typedef P priceType;
    typedef CakeType dessertType;

    Cake() = delete;

    /* CheeseCake ctor */
    template<class CT = CakeType>
    Cake(int initialStock, typename std::enable_if_t<!IsCream<CT>::value> * = nullptr)
            : stock_(initialStock) {
        static_assert(std::is_integral_v<T>, "T is not an integral!");
        static_assert(std::is_floating_point_v<P>, "P is not a floating point!");
        assert(initialStock > 0 && "Initial stock is not larger than 0!");
    }

    /* CreamCake ctor */
    template<class CT = CakeType>
    Cake(int initialStock, P price, typename std::enable_if_t<IsCream<CT>::value> * = nullptr)
            : stock_(initialStock), price_(price) {
        static_assert(std::is_integral_v<T>, "T is not an integral!");
        static_assert(std::is_floating_point_v<P>, "P is not a floating point!");
        assert(initialStock > 0 && "Initial stock is not larger than 0!");
    }

    constexpr static double getArea() {
        return length * width * ln2();
    }

    int getStock() {
        return stock_;
    }

    template<class CT = CakeType, class = typename std::enable_if_t<IsCream<CT>::value>>
    void sell() {
        assert(stock_ > 0 && "Stock is empty!");
        --stock_;
    }

    template<class CT = CakeType, class = typename std::enable_if_t<IsCream<CT>::value>>
    P getPrice() {
        return price_;
    }

private:
    int stock_;
    P price_;

    /* LN2 */
    constexpr static double fastExpRecursion(double x, int n, double res) {
        return n == 0 ? res :
               (n % 2 == 1 ? fastExpRecursion(x * x, n / 2, res * x)
               : fastExpRecursion(x * x, n / 2, res));
    }

    constexpr static double fastExponent(double x, int n) {
        return fastExpRecursion(x, n, 1);
    }

    constexpr static double ln2BbpFormula(int k, int n) {
        return 1.0 / (k * fastExponent(2.0, k)) + ((k == n) ? 0 : ln2BbpFormula(k + 1, n));
    }

    constexpr static int LN2_ALGORITHM_ITERATIONS = 20;

    constexpr static double ln2() {
        return ln2BbpFormula(1, LN2_ALGORITHM_ITERATIONS);
    }
};

template<class T, T length, T width> using CheeseCake = Cake<T, length, width, double, Cheese>;
template<class T, T length, T width, class P> using CreamCake = Cake<T, length, width, P, Cream>;

#endif // CAKE_H
