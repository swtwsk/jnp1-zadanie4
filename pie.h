#ifndef PIE_H
#define PIE_H

#include <type_traits>
#include <cassert>

class Apple;
class Cherry;

template<class T>
struct IsApple {
    constexpr static bool value = false;
};

template<>
struct IsApple<Apple> {
    constexpr static bool value = true;
};

template<class T>
constexpr bool isAppleV = IsApple<T>::value;

template<class R, R radius, class P, class PieType>
class Pie {
public:
    typedef R dimType;
    typedef P priceType;
    typedef PieType dessertType;

    Pie() = delete;

    /* CherryPie ctor */
    template<class PT = PieType>
    Pie(int initialStock, typename std::enable_if_t<!IsApple<PT>::value> * = nullptr)
            : stock_(initialStock) {
        static_assert(std::is_integral_v<R>, "R is not an integral!");
        static_assert(std::is_floating_point_v<P>, "P is not a floating point!");
        assert(initialStock > 0 && "Initial stock is not larger than 0!");
    }

    /* ApplePie ctor */
    template<class PT = PieType>
    Pie(int initialStock, P price, typename std::enable_if_t<IsApple<PT>::value> * = nullptr)
            : stock_(initialStock), price_(price) {
        static_assert(std::is_integral_v<R>, "R is not an integral!");
        static_assert(std::is_floating_point_v<P>, "P is not a floating point!");
        assert(initialStock > 0 && "Initial stock is not larger than 0!");
    }

    constexpr static double getArea() {
        return pi() * radius * radius;
    }

    int getStock() {
        return stock_;
    }

    template<class PT = PieType, class = typename std::enable_if_t<IsApple<PT>::value>>
    void sell() {
        assert(stock_ > 0 && "Stock is empty!");
        --stock_;
    }

    template<class PT = PieType, class = typename std::enable_if_t<IsApple<PT>::value>>
    P getPrice() {
        return price_;
    }

    template<class PT = PieType, class = typename std::enable_if_t<IsApple<PT>::value>>
    void restock(int additionalStock) {
        stock_ += additionalStock;
    }

private:
    int stock_;
    P price_;

    /* PI */
    constexpr static double fastExpRecursion(double x, int n, double res) {
        return n == 0 ? res :
               (n % 2 == 1 ? fastExpRecursion(x * x, n / 2, res * x) 
               : fastExpRecursion(x * x, n / 2, res));
    }

    constexpr static double fastExponent(double x, int n) {
        return fastExpRecursion(x, n, 1);
    }

    constexpr static double piBbpFormula(double k, int n) {
        return 1.0 / fastExponent(16.0, (int) k) *
               (4.0 / (8.0 * k + 1.0) - 2.0 / (8.0 * k + 4.0) - 1.0 / (8.0 * k + 5.0) -
                1.0 / (8.0 * k + 6.0))
               + (k == n ? 0 : piBbpFormula(k + 1.0, n));
    }

    constexpr static int PI_ALGORITHM_ITERATIONS = 10;

    constexpr static double pi() {
        return piBbpFormula(0, PI_ALGORITHM_ITERATIONS);
    }
};

template<class R, R radius> using CherryPie = Pie<R, radius, double, Cherry>;
template<class R, R radius, class P> using ApplePie = Pie<R, radius, P, Apple>;

#endif // PIE_H
